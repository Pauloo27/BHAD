# BHAD
**B**anco **H**orrível de **A**rmazenamento de **D**ados

## Storage
The database is stored in a files so it's persistent.

### Schema File

#### Info

**Extension**: `.bhads`  
**Magic Number**: `CAFE F710`

#### Doc

Stores the columns' name and types.

##### Types

- BYTE
  - Size: 8 bits
  - ID: 2
- ANSII CHAR **-> Extends Byte**
  - ID: 3

##### Format
{MAGIC NUMBER (4 bytes)}{COLUMN NAME CHARS COUNT (byte)}{COLUM NAME CHAR (ANSII CHAR)}{TYPE ID (byte)}

### Store File

#### Info

**Extesion**: `.bhad`  
**Magic Number**: `0xBEBA CAFE`

### Store File

#### Info

**Extension**: `.bhad`    
**Magic Number**: `BEBA CAFE`

#### Doc

Stores the data.

##### Format
{MAGIC NUMBER (4 bytes)}{COLUMN VALUE}

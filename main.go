package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type StoreFile struct {
	*os.File
	TotalSize int64
	Columns   []ColumnSchema
}

type ColumnType struct {
	Name      string
	Id, Bytes byte
}

type ColumnSchema struct {
	Name string
	Type ColumnType
}

var Types = map[byte]ColumnType{
	1: {Name: "BYTE", Id: 1, Bytes: 1},
	2: {Name: "ANSII CHAR", Id: 2, Bytes: 1},
	3: {Name: "SHORT", Id: 3, Bytes: 2},
}

func HandleWarning(message string) {
	log.Println(message)
}

func HandleFatal(err error, message string) {
	if err == nil {
		return
	}
	log.Fatal(fmt.Sprintf("%s: %s", message, err))
}

func CompareSlices(a, b []byte) bool {
	if len(a) != len(b) {
		return false
	}
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func pullBytes(buffer []byte, offset, byteCount uint) []byte {
	return buffer[offset : offset+byteCount]
}

func parseColumn(buffer []byte, offset *uint) ColumnSchema {
	nameSize := uint(pullBytes(buffer, *offset, 1)[0])
	*offset += 1
	name := string(pullBytes(buffer, *offset, nameSize))
	*offset += nameSize
	dataType := pullBytes(buffer, *offset, 1)[0]
	*offset += 1
	return ColumnSchema{Name: name, Type: Types[dataType]}
}

func loadSchemaFile(filePath string) (columns []ColumnSchema) {
	if !strings.HasSuffix(filePath, ".bhads") {
		HandleWarning("Schema file doesn't have the .bhads extension")
	}

	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		HandleFatal(fmt.Errorf("Schema file %s not exist", filePath), "Invalid schema file")
	}

	file, err := os.Open(filePath)
	HandleFatal(err, "Cannot open schema file")
	data, err := ioutil.ReadAll(file)
	HandleFatal(err, "Cannot read schema file")

	dataSize := uint(len(data))

	if dataSize <= 4 {
		HandleFatal(fmt.Errorf("Missing magic number or content"), "File too short")
	}

	magicNumber := pullBytes(data, 0, 4)

	if !CompareSlices(magicNumber, []byte{0xCA, 0xFE, 0xF7, 0x10}) {
		HandleFatal(fmt.Errorf("The magic number should be CAFE F710"), "Invalid magic number")
	}

	offset := uint(4)
	for offset < dataSize {
		columns = append(columns, parseColumn(data, &offset))
	}

	return
}

func loadStoreFile(filePath string, columns []ColumnSchema) *StoreFile {
	if !strings.HasSuffix(filePath, ".bhad") {
		HandleWarning("Store file doesn't have the .bhad extension")
	}

	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		HandleFatal(fmt.Errorf("Store file %s not exist", filePath), "Invalid store file")
	}

	file, err := os.OpenFile(filePath, os.O_RDWR, 0755)
	HandleFatal(err, "Cannot open store file")

	info, err := file.Stat()
	HandleFatal(err, "Cannot stat store file")

	return &StoreFile{Columns: columns, File: file, TotalSize: info.Size()}
}

func (file *StoreFile) PullByte(offset int64) byte {
	buffer := []byte{0}
	_, err := file.ReadAt(buffer, offset)
	HandleFatal(err, "Cannot read from store file")
	return buffer[0]
}

func (store *StoreFile) GetRowOffset() (offset int) {
	for _, column := range store.Columns {
		offset += int(column.Type.Bytes)
	}
	return
}

func (store *StoreFile) SetRowValue(id int, data []byte) {
	var offset int64
	if id <= 0 {
		offset = int64(store.GetRowOffset())
		lastId := (store.TotalSize - 4) / offset
		store.SetRowValue(int(lastId+1), data)
		store.TotalSize += offset
		return
	} else {
		offset = int64(4 + (id-1)*store.GetRowOffset())
	}
	_, err := store.Seek(offset, 0)
	HandleFatal(err, "Cannot write to store file")
	_, err = store.Write(data)
	HandleFatal(err, "Cannot write to store file")
}

func (store *StoreFile) GetRowValue(id int) (row [][]byte) {
	columnsCount := len(store.Columns)
	offset := 4 + (id-1)*store.GetRowOffset()

	for i := 0; i < columnsCount; i += 1 {
		column := store.Columns[i]
		var data []byte
		for x := 0; byte(x) < column.Type.Bytes; x += 1 {
			data = append(data, store.PullByte(int64(offset)))
			offset += 1
		}
		row = append(row, data)
	}

	return
}

type FindFilter func([][]byte) bool

func (store *StoreFile) FindRows(limit uint, filter FindFilter) (rows [][][]byte) {
	lastId := (store.TotalSize - 4) / int64(store.GetRowOffset())
	for i := int64(1); i <= lastId; i += 1 {
		if limit != 0 && len(rows) >= int(limit) {
			break
		}
		row := store.GetRowValue(int(i))
		if filter == nil || filter(row) {
			rows = append(rows, row)
		}
	}
	return
}

func (store *StoreFile) AppendRowValue(data []byte) {
	store.SetRowValue(0, data)
}

func main() {
	columns := loadSchemaFile("main.bhads")
	store := loadStoreFile("main.bhad", columns)

	rows := store.FindRows(0, nil)
	fmt.Println(rows)
}
